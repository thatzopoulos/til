# TIL
> Things I Learned
Collection of Software Engineering Facts / Shortcuts.
---
### intellij

- [Useful Intellij Shortcuts](intellij/useful-intellij-shortcuts.md)

### python

- [My Python Environment](python/my-python-environment.md)

### rust

- [Collect Std Env Args](rust/collect-std-env-args.md)

### unix

- [Clear Screen](unix/clear-screen.md)

### vscode

- [Advance Through Search Results](vscode/advance-through-search-results.md)

### workflow

- [Open Slacks Keyboard Shortcuts Reference Panel](workflow/open-slacks-keyboard-shortcuts-reference-panel.md)

