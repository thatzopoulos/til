# Collect Arguments Given To Rust Program Using Standard Env

Collect all of the args given to a rust program without external crates;

```    let args: Vec<String> = std::env::args().collect();
 ```