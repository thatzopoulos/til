# Clear The Screen

If you type `clear` into your shell, the screen will be cleared.

`ctrl-l` achieves the same effect.
