# Advance Through Search Results

Hit `F4` to move forward one by one through the search results.

To move backward through the results, hit `Shift+F4`.