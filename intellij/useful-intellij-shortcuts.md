# Essential Intellij Shortcuts

# Top Two
## Ctrl+Click
When clicking on a method call: Jump to the method’s implementation
When clicking on a method signature: See usages of the method.

## Ctrl+Shift+A
Search in Actions. 

Each action in the result list is accompanied by its shortcut which helps learning.

## Ctrl+Shift+N
Search in File Names (and Paths). 

## F2
Jump to the next problem/IDE suggestion. 

## Ctrl+E
Show the list of recent files. This could possibly become my replacement for Ctrl+Tab, as this is what I want most of the time - and is easier on my fingers. (Thanks Benjamin!)
## Ctrl+Shift+E
Similar to Ctrl+E, but shows all recent positions incl. the surrounding lines.
